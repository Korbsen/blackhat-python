"""
author: KorbZen

Description:
    This file is used to show why you shouldn't trust a pickle file.
"""

import pickle
import os
from sys import platform

class malicious():
    
    def __init__(self):
        """Constructor
        This is the constructor of our malicious class.
        
        Purpose
        -------
        We are going to use this to open and run a bash console on instantiation
        
        From that point on you could open a new 
        """
        print("Gotcha")
        
        # open a console if mac system
        if platform == "darwin":
            print("Nice mac you got there")
            print("Let's check your files:")
            os.system("ls")
        
        # open console on windows system
        elif platform == "win32":
            os.system("ls")

    def __reduce__(self):
        """
        This function defines what happens when Pickler reduces an object.
        """
        # you can define inputs in the second tuple if you wish to
        return (self.__class__, ())


if __name__ == "__main__":
    
    # we are going to instatiate our object here
    t = malicious()
    # for testing we are hiding our object in a list
    SomeList = [t, 1, 2, 'h']
    
    # now let's save it
    with open("someObject.pkl", "wb") as f:
        pickle.dump(SomeList, f)

    del SomeList
    
    # and now let's load the file
    with open("someObject.pkl", "rb") as f:
        pickle.load(f)
    
    